package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

// Geralmente não implementamos ObjectMother isoladamente. Colocamos nossa lógica de ObjectMother dentro do Test Data Builder.
// Usualmente, as instâncias que seriam fornecidas pelo ObjectMother, nós configuramos dentro do Test Data Builder.
public class AlunoMother {

	private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;
	private static final String NOME_DEFAULT = "Claudio Neiva";
	private static final int ANO_NASCIMENTO_DEFAULT = 2000;
	private static final int MATRICULA_DEFAULT = 10;

	public static Aluno umAluno() {
		Aluno aluno1 = new Aluno();
		aluno1.setSituacao(SITUACAO_DEFAULT);
		aluno1.setMatricula(MATRICULA_DEFAULT);
		aluno1.setAnoNascimento(ANO_NASCIMENTO_DEFAULT);
		aluno1.setNome(NOME_DEFAULT);
		return aluno1;
	}

	public static Aluno umAlunoCancelado() {
		Aluno aluno1 = new Aluno();
		aluno1.setSituacao(SituacaoAluno.CANCELADO);
		aluno1.setMatricula(MATRICULA_DEFAULT);
		aluno1.setAnoNascimento(ANO_NASCIMENTO_DEFAULT);
		aluno1.setNome(NOME_DEFAULT);
		return aluno1;
	}

	public static Aluno umAlunoMaiorIdade() {
		Aluno aluno1 = new Aluno();
		aluno1.setSituacao(SITUACAO_DEFAULT);
		aluno1.setMatricula(MATRICULA_DEFAULT);
		aluno1.setAnoNascimento(1973);
		aluno1.setNome(NOME_DEFAULT);
		return aluno1;
	}

}
