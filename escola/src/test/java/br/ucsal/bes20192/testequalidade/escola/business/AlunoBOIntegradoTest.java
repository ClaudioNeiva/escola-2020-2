package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private static AlunoDAO alunoDAO = new AlunoDAO();
	private static DateHelper dateUtil = new DateHelper();
	private static AlunoBO alunoBO = new AlunoBO(alunoDAO, dateUtil);

	// @BeforeAll
	public static void setupAll() {
		alunoDAO = new AlunoDAO();
		dateUtil = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateUtil);
	}

	@BeforeEach
	public void setup() {
		alunoDAO.excluirTodos();
	}

	@AfterAll
	public static void tearDownAll() {
		alunoDAO.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 tera 16
	 * anos. Caso de teste # | entrada | saida esperada 1 | aluno nascido em 2003 |
	 * * 16
	 */
	public void testarCalculoIdadeAluno1() {
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
	}

}
